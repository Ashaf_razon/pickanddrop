package com.avis.avistechbd.pickanddrop.Activities;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class BestSplashTheme extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        Intent goWapSlider = new Intent(BestSplashTheme.this,MainActivity.class);
        startActivity(goWapSlider);
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
