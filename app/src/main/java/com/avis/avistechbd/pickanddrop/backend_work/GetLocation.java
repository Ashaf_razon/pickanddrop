package com.avis.avistechbd.pickanddrop.backend_work;

public interface GetLocation {
    void getLocationOnce(String myLatt, String myLong, int didUpdate);
}
