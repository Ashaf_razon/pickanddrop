package com.avis.avistechbd.pickanddrop.backend_work;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.avis.avistechbd.pickanddrop.R;

public class GetSetUserDataToLocNfirebase implements LocationListener {
    //ConstantVar
    private final double OFFICE_LATT = 23.750104;
    private final double OFFICE_LONG = 90.392888;
    int didUpdate = 0;
    /*
        )OnMapCallBack
        )LocationListener for UpdateLocation
        )ReSend Update LattLong to the parent
    */

    //Get_App_Context
    private Context context;
    //Get_App_Activity
    private Activity getActivity;
    private GetLocation getMyLocation;
    //get_Location
    private LocationManager locationManager;
    //Constant
    private static final int REQUEST_LOCATION = 1;
    private static final int MIN_UPDATE_TIME = 2000;
    private static final int MIN_UPDATE_DISTANCE = 0;

    public GetSetUserDataToLocNfirebase(LocationManager locationManager,Context context,Activity getActivity,GetLocation getMyLocation) {
        this.context = context;
        this.getActivity = getActivity;
        this.getMyLocation = getMyLocation;
        this.locationManager = locationManager;
    }


    public void getLocation() {
        double latti = 0.0;
        double longi = 0.0;
        Location location = null, location1 = null,location2 = null, location3 = null;

        try{
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                    (context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                ActivityCompat.requestPermissions(getActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);
            }
        }catch (Exception e){
            Toast.makeText(context, "FINE_LOC: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        try{
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }catch (Exception e){
            e.printStackTrace();
        }try{
            location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            location3 = locationManager.getLastKnownLocation(LocationManager.KEY_LOCATION_CHANGED);
        }catch (Exception e){
            e.printStackTrace();
        }

        Toast.makeText(context, "Loc: "+location+" >> "+location1 +" >> "+location2, Toast.LENGTH_SHORT).show();

        if (location != null) {
            try {
                didUpdate = 1;
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
                latti = location.getLatitude();
                longi = location.getLongitude();
            } catch (Exception e) {
                e.printStackTrace();
                //Toast.makeText(context, "0: "+R.string.un_network, Toast.LENGTH_SHORT).show();
            }
        } else if (location1 != null) {
            try {
                didUpdate = 1;
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
                latti = location1.getLatitude();
                longi = location1.getLongitude();
            } catch (Exception e) {
                e.printStackTrace();
               // Toast.makeText(context, "1: "+R.string.un_gps, Toast.LENGTH_SHORT).show();
            }
        } else if ( location2 != null) {
            try {
                didUpdate = 1;
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
                latti = location2.getLatitude();
                longi = location2.getLongitude();
            } catch (Exception e) {
                e.printStackTrace();
                //Toast.makeText(context, "2: "+R.string.un_passive_loc, Toast.LENGTH_SHORT).show();
            }
        }else if ( location3 != null) {
            try {
                didUpdate = 1;
                locationManager.requestLocationUpdates(LocationManager.KEY_LOCATION_CHANGED, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
                latti = location3.getLatitude();
                longi = location3.getLongitude();
            } catch (Exception e) {
                e.printStackTrace();
                //Toast.makeText(context, "2: "+R.string.un_passive_loc, Toast.LENGTH_SHORT).show();
            }
        } else {
            didUpdate = 0;
            latti = OFFICE_LATT;
            longi = OFFICE_LONG;
            //Toast.makeText(context, R.string.un_trace_loc, Toast.LENGTH_SHORT).show();
        }
        //Send_My_Current_Location_Once
        getMyLocation.getLocationOnce(String.valueOf(latti), String.valueOf(longi), didUpdate);
    }

    @Override
    public void onLocationChanged(Location location) {
        getMyLocation.getLocationOnce(String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),didUpdate);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(context,R.string.gps_enable_status,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(context,R.string.gps_disable_status,Toast.LENGTH_LONG).show();
    }
}
