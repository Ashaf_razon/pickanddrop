package com.avis.avistechbd.pickanddrop.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.pickanddrop.R;
import com.avis.avistechbd.pickanddrop.create_view.ListPassengerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivePassengerList extends AppCompatActivity {
    //Constant
    private final int ACTIVE_LIST = 1;
    private final int NOT_ACTIVE_LIST = 0;

    //ViewBind
    @BindView(R.id.countGoingPassenger)
    TextView countGoingPassenger;
    @BindView(R.id.countNotGoingPassenger)
    TextView countNotGoingPassenger;
    //ActivityVariables
    private Context context;
    private Handler handler;
    private ProgressDialog pd;
    //FirebaseDB
    private FirebaseDatabase database;
    //LocalDB
    private SharedPreferences mySharedPref;
    private String myRef = "userStatus"; //Reference_Key

    //DataArrayList
    private final String TAG = "ActivePassengers";
    private ArrayList<String> pasngrName = new ArrayList<>();
    private ArrayList<String> pasngrPhone = new ArrayList<>();
    private ArrayList<String> pickPoint = new ArrayList<>();
    private ArrayList<String> pickTIme = new ArrayList<>();

    //address
    private String city, area, add;
    //Passenger count
    int notGoing = 0, yesGoing = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_passenger_list);
        ButterKnife.bind(this);
        setTitle("Today's Passengers");
        //initialize
        context = ActivePassengerList.this;
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(context);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        database = FirebaseDatabase.getInstance();
        //localDB
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        callFireListOfPassengers(ACTIVE_LIST);
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        ListPassengerView adapter = new ListPassengerView(pasngrName, pasngrPhone, pickPoint, pickTIme, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void callFireListOfPassengers(final int setListType) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DatabaseReference reference = database.getInstance().getReference("HiaceCar");
                    Query query = reference.child("PassengerList");
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                notGoing = 0;
                                yesGoing = 0;
                                for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                    // Log.d("LogIn","snap_2: "+issue);
                                    if(!issue.child("PickTime").getValue().toString().equals("0.00 am/pm")){
                                        if(setListType == 1){
                                            if (issue.child("Going").getValue().toString().equals("1")) {
                                                yesGoing += 1;
                                                pasngrPhone.add("Phone: " + issue.getKey());
                                                pasngrName.add("Name: " + issue.child("Name").getValue().toString());
                                                pickPoint.add("Pick Point: " + findMyAddress(Double.valueOf(issue.child("uLatt").getValue().toString()), Double.valueOf(issue.child("uLong").getValue().toString())));
                                                pickTIme.add("Pick Time: " + issue.child("PickTime").getValue().toString());
                                            } else {
                                                notGoing += 1;
                                            }
                                        }else{
                                            if (issue.child("Going").getValue().toString().equals("0")) {
                                                notGoing += 1;
                                                pasngrPhone.add("Phone: " + issue.getKey());
                                                pasngrName.add("Name: " + issue.child("Name").getValue().toString());
                                                pickPoint.add("Pick Point: " + findMyAddress(Double.valueOf(issue.child("uLatt").getValue().toString()), Double.valueOf(issue.child("uLong").getValue().toString())));
                                                pickTIme.add("Pick Time: " + issue.child("PickTime").getValue().toString());
                                            } else {
                                                yesGoing += 1;
                                            }
                                        }
                                    }
                                }
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            countGoingPassenger.setText("Going : " + yesGoing);
                                            countNotGoingPassenger.setText("Not Going : " + notGoing);
                                            initRecyclerView();
                                            pd.dismiss();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            pd.dismiss();
                                        }
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Toast.makeText(context, "Sorry, No Passenger found", Toast.LENGTH_SHORT).show();
                                            pd.dismiss();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            pd.dismiss();
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            pd.dismiss();
                            Toast.makeText(context, "SignIn DB Error, Try again latter ", Toast.LENGTH_SHORT).show();
                        }
                    });
                    Thread.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Log.d("LogIn", "End");
    }

    private String findMyAddress(double lt, double lng) {
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        add = "Not found";
        try {
            addresses = gcd.getFromLocation(lt, lng, 1);
            city = addresses.get(0).getLocality();
            area = addresses.get(0).getFeatureName();
            add = addresses.get(0).getSubLocality() + ", " + addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return add;
    }

    @OnClick({R.id.seeGoing, R.id.seeNotGoing})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.seeGoing:
                arrayListInit();
                callFireListOfPassengers(ACTIVE_LIST);
                break;
            case R.id.seeNotGoing:
                arrayListInit();
                callFireListOfPassengers(NOT_ACTIVE_LIST);
                break;
        }
    }

    private void arrayListInit() {
        pasngrName = new ArrayList<>();
        pasngrPhone = new ArrayList<>();
        pickPoint = new ArrayList<>();
        pickTIme = new ArrayList<>();
    }
}
