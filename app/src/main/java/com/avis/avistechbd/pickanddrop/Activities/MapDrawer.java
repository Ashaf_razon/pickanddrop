package com.avis.avistechbd.pickanddrop.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.pickanddrop.R;
import com.avis.avistechbd.pickanddrop.backend_work.GetLocation;
import com.avis.avistechbd.pickanddrop.backend_work.GetSetUserDataToLocNfirebase;
import com.avis.avistechbd.pickanddrop.backend_work.SendGetLocToFromFirebase;
import com.avis.avistechbd.pickanddrop.custom_dialog.CustomDialogTwo;
import com.avis.avistechbd.pickanddrop.custom_dialog.mDialogClass;
import com.avis.avistechbd.pickanddrop.get_location_backend.CheckUserPermission;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapDrawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GetLocation {
    //Constant
    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_PHONE_CALL = 1;
    private static final int GET_PICK_POINT_ON_CREATE = 0;
    private static final int GET_PICK_POIINT_ON_LOC_CHANGE = 1;
    private static final String PASSENGER_REF = "PassengerList";
    private static final String PICK_POINT_REF = "PickUpPoint";
    //ViewBinding
    @BindView(R.id.chargeSearv)
    TextView chargeSearv;
    @BindView(R.id.ratingServ)
    TextView ratingServ;
    @BindView(R.id.addrsSearv)
    TextView addrsSearv;
    //FirebaseDB
    private FirebaseDatabase database;
    //LocalDB
    private SendGetLocToFromFirebase userInfo;
    private SharedPreferences mySharedPref;
    private String myRef = "userStatus"; //Reference_Key
    //Variables
    private String mobLieNoGet = "01717000000";//Control_Spinner_Flag
    private int arrWCl = 2;

    //ViewBinding
    @BindView(R.id.u_detection)
    ImageView uDetection;
    @BindView(R.id.upArraw)
    ImageView upArraw;
    private Context context;
    private Handler handler;
    private ProgressDialog pd;
    private int getUserTyepFlag;
    private ImageView setImageAdmin;
    private TextView headerName;
    //BottomSheet
    BottomSheetBehavior behavior;
    //MapView
    private GoogleMap mMap;
    private Marker myMarker;
    private SupportMapFragment mapFragment;
    //MultiMarker
    private MarkerOptions options;
    //Location_Permission
    private CheckUserPermission getMyPermission; //Permission_Check
    private LocationManager locationManager;
    private GetSetUserDataToLocNfirebase getMyAllLoc; //class_Object
    //address
    private String city, area, add;
    //UserDataGetSet_PiCK_POINT_flag
    //DataArrayList
    private final String TAG = "MapDrawer";
    private ArrayList<String> pasngrName = new ArrayList<>();
    private ArrayList<String> pasngrPhone = new ArrayList<>();
    private ArrayList<String> pickPoint = new ArrayList<>();//Shows_going_status
    private ArrayList<String> pickTime = new ArrayList<>();
    private ArrayList<String> picLatt = new ArrayList<>();
    private ArrayList<String> pickLong = new ArrayList<>();
    private String driverLatt = "";
    private String driverLong = "";
    private String driverPhone = "";
    String driverName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_drawer);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //HomeFragmentView
        View hView = navigationView.getHeaderView(0);
        setImageAdmin =hView.findViewById(R.id.imageViewAdmin);
        headerName = hView.findViewById(R.id.accountName);
        getUserTyepFlag = getIntent().getIntExtra("UserType", 1);
        //InitView
        initInitialView();
        try {
            if (getUserTyepFlag == 0) {
                setImageAdmin.setImageDrawable(ContextCompat.getDrawable(MapDrawer.this, R.drawable.driver));
                uDetection.setImageDrawable(ContextCompat.getDrawable(MapDrawer.this, R.drawable.avis_logo));
            } else {
                setImageAdmin.setImageDrawable(ContextCompat.getDrawable(MapDrawer.this, R.drawable.profile));
            }
        } catch (Exception e) {
            Log.d("headerImage", getUserTyepFlag + " " + e.getMessage());
        }
    }
    private void emptyArraylistField() {
        try {
            if (!pasngrName.isEmpty()) {
                pasngrName.clear();
            }
            if (!pasngrPhone.isEmpty()) {
                pasngrPhone.clear();
            }
            if (!pickPoint.isEmpty()) {
                pickPoint.clear();
            }
            if (!picLatt.isEmpty()) {
                picLatt.clear();
            }
            if (!pickLong.isEmpty()) {
                pickLong.clear();
            }
            if (!pickTime.isEmpty()) {
                pickTime.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void initInitialView() {
        bottomSheetInit();
        try{
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        context = MapDrawer.this;
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(context);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        options = new MarkerOptions();
        database = FirebaseDatabase.getInstance();
        //localDB
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        userInfo = new SendGetLocToFromFirebase(context, mySharedPref, database);
        initMapView();//MapView
        //initially_users_Location_view
        try {
            initLocationManagerPermission();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //initialize_map_all_PiCK_point
        try{
            pd.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        Thread.sleep(2000);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                //callFireListOfPassengers(OFFICE_LATT,OFFICE_LONG,GET_PICK_POINT_ON_CREATE);
                                getPermissionInfoNLocation();
                                Log.d(TAG,"init_Create_1: "+pasngrPhone.size());
                                pd.dismiss();
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }).start();
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            headerName.setText(userInfo.getLocalUserName());
        } catch (Exception e) {
            Log.d(TAG+"headerName",""+e.getMessage());
        }
    }

    private void bottomSheetInit() {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.myMainMapLay);
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet2);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        getPermissionInfoNLocation();
                        arrWCl = 3;
                        upArraw.setImageDrawable(getResources().getDrawable(R.drawable.ic_dwn_arr));
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        arrWCl = 2;
                        upArraw.setImageDrawable(getResources().getDrawable(R.drawable.ic_upr_arr));
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        Button button = findViewById(R.id.callServe);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                if (mobLieNoGet.toString().equals("01717000000") != true)
                    toServPhoneCallEnabe();
                else
                    Toast.makeText(context, R.string.pickPoint_alert, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Change State onClick UpArrow
    @OnClick(R.id.upArraw)
    public void onUpArrawClicked() {
        try{
            getPermissionInfoNLocation();
            if (arrWCl % 2 == 0) {
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                upArraw.setImageDrawable(getResources().getDrawable(R.drawable.ic_dwn_arr));
                arrWCl = 3;
            } else if (arrWCl % 2 == 1) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                upArraw.setImageDrawable(getResources().getDrawable(R.drawable.ic_upr_arr));
                arrWCl = 2;
            }
        }catch (Exception e){
            Log.d("shomap_bottomSheet",""+e.getMessage());
        }
    }

    private void toServPhoneCallEnabe() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        try {
            callIntent.setData(Uri.parse("tel:" + mobLieNoGet));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            customDialogCall(R.string.dialog_title_log_out);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map_drawer, menu);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.pickStatus) {
            // Handle the camera action
            carriedIntentMethod(1);
        } else if (id == R.id.setNewPickLocation) {
            carriedIntentMethod(2);
        } else if (id == R.id.chat) {
            carriedIntentMethod(3);
        } else if (id == R.id.notifications) {
            carriedIntentMethod(4);
        } else if (id == R.id.todaysPassengers) {
            carriedIntentMethod(5);
        } else if (id == R.id.allPassengers) {
            carriedIntentMethod(6);
        } else if (id == R.id.logOut) {
            customDialogCall(R.string.dialog_title_log_out);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void customDialogCall(int setTitle) {
        mDialogClass myDialog = new mDialogClass((Activity) context, setTitle, 1, context, database, mySharedPref);
        try {
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }

    private void customDialogAlert(int setTitle) {
        CustomDialogTwo myDialog = new CustomDialogTwo((Activity) context, setTitle, 1);
        try {
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        myDialog.setCancelable(true);
        myDialog.show();
    }

    private void carriedIntentMethod(final int setId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            switch (setId) {
                                case 1:
                                    try {
                                        if (getUserTyepFlag == 0) {
                                            customDialogAlert(R.string.dialog_title_Driver_Alert);
                                        } else {
                                            Intent getPicStatus = new Intent(context, SetPickStatus.class);
                                            startActivity(getPicStatus);
                                        }
                                    } catch (Exception e) {
                                        Toast.makeText(context, "Sorry! System error, Try again latter", Toast.LENGTH_LONG).show();
                                    }
                                    break;
                                case 2:
                                    try {
                                        if (getUserTyepFlag == 0) {
                                            customDialogAlert(R.string.dialog_title_Driver_Alert);
                                        } else {
                                            Intent getPickPoint = new Intent(context, SetNewPickPoint.class);
                                            startActivity(getPickPoint);
                                        }
                                    } catch (Exception e) {
                                        Toast.makeText(context, "Sorry! System error, Try again latter", Toast.LENGTH_LONG).show();
                                    }
                                    break;
                                case 3:
                                    try {
                                        if (getUserTyepFlag == 0) {
                                            customDialogAlert(R.string.dialog_title_Driver_Alert);
                                        } else {
                                            customDialogAlert(R.string.dialog_title_Msg);
                                        }
                                    } catch (Exception e) {
                                        Toast.makeText(context, "Sorry! System error, Try again latter", Toast.LENGTH_LONG).show();
                                    }
                                    break;
                                case 4:
                                    try {
                                        if (getUserTyepFlag == 0) {
                                            customDialogAlert(R.string.dialog_title_Driver_Alert);
                                        } else {
                                            customDialogAlert(R.string.dialog_title_Notifications);
                                        }
                                    } catch (Exception e) {
                                        Toast.makeText(context, "Sorry! System error, Try again latter", Toast.LENGTH_LONG).show();
                                    }
                                    break;
                                case 5:
                                    try {
                                        Intent activeListPassenger = new Intent(context, ActivePassengerList.class);
                                        startActivity(activeListPassenger);
                                    } catch (Exception e) {
                                        Toast.makeText(context, "Sorry! System error, Try again latter", Toast.LENGTH_LONG).show();
                                    }
                                    break;
                                case 6:
                                    try {
                                        Intent allListPassenger = new Intent(context, AllPassengersList.class);
                                        startActivity(allListPassenger);
                                    } catch (Exception e) {
                                        Toast.makeText(context, "Sorry! System error, Try again latter", Toast.LENGTH_LONG).show();
                                    }
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    //call_On_Loc_Change = 1 & _On_create_Method = 0
    private void callFireListOfPassengers(final double officeLat, final double officeLong, int methodCallStatus) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DatabaseReference reference = database.getInstance().getReference("HiaceCar");
                    Query query = reference.child(PASSENGER_REF);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                emptyArraylistField();
                                for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                    try{
                                        if (issue != null) {
                                            try{
                                                Log.d(TAG+"issue",""+issue);
                                                if(issue.child("Name").getValue().toString().equals("Captain: RUBEL")){
                                                    driverLatt = issue.child("uLatt").getValue().toString();
                                                    driverLong = issue.child("uLong").getValue().toString();
                                                    driverName = "RUBEL";
                                                    driverPhone = issue.getKey().toString();
                                                }else {
                                                    pasngrPhone.add(issue.getKey().toString());
                                                    pasngrName.add(issue.child("Name").getValue().toString());
                                                    //passengers_going_status
                                                    pickPoint.add(issue.child("Going").getValue().toString());
                                                    pickTime.add(issue.child("PickTime").getValue().toString());
                                                    picLatt.add(issue.child("uLatt").getValue().toString());
                                                    pickLong.add(issue.child("uLong").getValue().toString());
                                                }
                                            }catch (Exception e){
                                                Log.d(TAG+"_getFire_Latt_Long",""+e.getMessage());
                                            }
                                        } else {
                                            Log.d(TAG,""+issue);
                                        }
                                    }catch (Exception e){
                                        Log.d(TAG,""+e.getMessage());
                                    }
                                }
                                try{
                                    showOnMap(officeLat,officeLong,picLatt, pickLong,pasngrPhone,pasngrName,
                                            null,1,Integer.valueOf(userInfo.getLocalUserType()),
                                            driverLatt,driverLong,driverName);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                Log.d(TAG+" MAP","array size_Latt: "+picLatt.size());
                                Log.d(TAG+" MAP","array size_Long: "+pickLong.size());
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Toast.makeText(context, "Sorry, No PickUp point found", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(context, "SignIn DB Error, Try again latter ", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Log.d("DraweMap", "End");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
    }

    private void setCustomerMarker(MarkerOptions options, GoogleMap googleMap, double lt, double lng, int pickFlag, int usersStatus) {
        options.position(new LatLng(lt, lng));
        if(pickFlag == 0 && usersStatus == 1){
            options.title("Office");
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        }else if(pickFlag == 1 && usersStatus == 1){
            options.title("My Position");
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        }else{
            options.title("Hiace Car");
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        }
        options.snippet(userInfo.getLocalUserName());
        myMarker = googleMap.addMarker(options);
    }

    //this will call EVERY_TIME when user's location changed
    @Override
    public void getLocationOnce(final String myLatt, final String myLong, int didUpdate) {
        try {
            if(didUpdate == 1){
                try{
                    //only Captain Location Update to FireDB
                    if(userInfo.getLocalUserType().equals("0")){
                        //String ref = "HiaceCar/Captain/"+userInfo.getLocalPhone();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    String ref2 = "HiaceCar/"+PASSENGER_REF+"/"+userInfo.getLocalPhone();//continuous
                                    userInfo.updateUsersPosition(ref2,myLatt,myLong);
                                    Thread.sleep(1000);
                                }catch (Exception e){
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            addrsSearv.setText("Location ref error");
                                        }
                                    });
                                }
                            }
                        }).start();
                        //(Double.valueOf(myLatt),Double.valueOf(myLong),GET_PICK_POIINT_ON_LOC_CHANGE);
                    }
                    //Passengers_Map
                    callFireListOfPassengers(Double.valueOf(myLatt),Double.valueOf(myLong),GET_PICK_POIINT_ON_LOC_CHANGE);

                }catch (Exception e){
                    addrsSearv.setText("location is unable to Trace."+e.getMessage());
                }
            }else {
                callFireListOfPassengers(Double.valueOf(myLatt),Double.valueOf(myLong),GET_PICK_POINT_ON_CREATE);
                addrsSearv.setText("We unable to Trace you.");
            }
        } catch (Exception e) {
            addrsSearv.setText("Error moving location: " + e.getMessage());
        }
    }

    private void getPermissionInfoNLocation() {
        if (getMyPermission.checkGPSpermission()) {
            //Done_Getting_All_Permission_(Location & GPS)
            try {
                if (getMyPermission.isNetworkAvailable()) {
                    //Get_Location_Once
                    try{
                        getMyAllLoc.getLocation();
                    }catch (Exception e){
                        Toast.makeText(this, "Permission interface: "+e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    //Toast.makeText(this, R.string.internet_msg, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Permission: "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.gps_msg, Toast.LENGTH_SHORT).show();
        }
    }

    private void initLocationManagerPermission() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                callForRequestPermission();
                //return;
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.location_manager_init_error, Toast.LENGTH_SHORT).show();
        }
        //get_Location_&_Permission_instance
        getMyAllLoc = new GetSetUserDataToLocNfirebase(locationManager, context, (Activity) context, (GetLocation) MapDrawer.this);
        getMyPermission = new CheckUserPermission(context, (Activity) context, locationManager);
    }

    private void callForRequestPermission() {
        ActivityCompat.requestPermissions(MapDrawer.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        ActivityCompat.requestPermissions(MapDrawer.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);
    }

    private String findMyAddress(double lt, double lng) {
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        add = "Not found";
        try {
            addresses = gcd.getFromLocation(lt, lng, 1);
            city = addresses.get(0).getLocality();
            area = addresses.get(0).getFeatureName();
            add = addresses.get(0).getSubLocality() + ", " + addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return add;
    }

    //pickUpflag = 0 (onCREATE_call -> View pickUp point)
    //pickUpflag = 1 (onLocation_Changed_Call -> View passenger & PickUp point)
    //usersStatus = 0 (Driver)
    //usersStatus = 1 (Passenger)
    private void showOnMap(final double lt, final double lng, final ArrayList<String> latAr,
                           final ArrayList<String> lngAr, final ArrayList<String> phnNoPass,
                           final ArrayList<String> namePass, final ArrayList<String> pickTimePass,
                           final int pickUpFlag, final int usersStatus, final String driverLattMap,
                           final String driverLongMap, final String driverNameMap) {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                try {
                    try {
                        googleMap.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //init_map_MARKER_view
                    try {
                        if (pickUpFlag == 0 && usersStatus == 1) {
                            try{
                                //seeOffice_LOCATION
                                setCustomerMarker(options, googleMap, lt, lng, pickUpFlag,usersStatus);
                                //Toast.makeText(context,"in_map",Toast.LENGTH_LONG).show();
                            }catch (Exception e){
                                Log.d("showmap_tryMarker_office_loc",""+e.getMessage());
                            }
                        } else {
                            //map_view_Passengers_Position
                            if (phnNoPass.isEmpty() || phnNoPass == null) {
                                try{
                                    setCustomerMarker(options, googleMap, lt, lng, pickUpFlag,usersStatus);
                                    Toast.makeText(MapDrawer.this,"No PickUp point found", Toast.LENGTH_LONG).show();
                                }catch (Exception e){
                                    Log.d("showmap_tryMarker_Passengers_loc",""+e.getMessage());
                                }
                            } else {
                                for (int x = 0; x <= phnNoPass.size()+1; x++) {
                                    try{
                                        //last position is users position
                                        if (x < phnNoPass.size()) {
                                            options.position(new LatLng(Double.parseDouble(latAr.get(x)), Double.parseDouble(lngAr.get(x))));
                                            options.title(namePass.get(x));
                                            options.snippet(phnNoPass.get(x));
                                            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                                            myMarker = googleMap.addMarker(options);
                                        } else if(x == phnNoPass.size()){
                                            options.position(new LatLng(Double.valueOf(driverLattMap), Double.valueOf(driverLongMap)));
                                            options.title("Captain");
                                            options.snippet(""+driverNameMap);
                                            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                                            myMarker = googleMap.addMarker(options);
                                        }else if(x == phnNoPass.size()+1 && usersStatus == 1){
                                            //Toast.makeText(context,"in_map999",Toast.LENGTH_LONG).show();
                                            //mode->passenger only
                                            setCustomerMarker(options, googleMap, lt, lng, pickUpFlag,usersStatus);
                                        }
                                    }catch (Exception e){
                                        Log.d("showmap_tryMarker_all_passengers",""+e.getMessage());
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.d("showmap_tryMarker",""+e.getMessage());
                    }
                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            markerClickMethod(marker,lt,lng);
                            return false;
                        }
                    });
                } catch (Exception e) {
                    Toast.makeText(MapDrawer.this, "Error Multiple Marker", Toast.LENGTH_LONG).show();
                }
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lt, lng), 14));
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
            }
        });
    }
    private void initMapView() {
        try {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void markerClickMethod(Marker marker,double usersLt, double usersLng) {
        try {
            LatLng getLatLngMarker = marker.getPosition();
            double mrLt = getLatLngMarker.latitude;
            double mrLn = getLatLngMarker.longitude;
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            String mrGetTitle = marker.getTitle();
            Location loc1 = null,loc2 = null;
            try{
                loc1 = new Location("");
                loc1.setLatitude(Double.valueOf(usersLt));
                loc1.setLongitude(Double.valueOf(usersLng));
                loc2 = new Location("");
                loc2.setLatitude(mrLt);
                loc2.setLongitude(mrLn);
            }catch (Exception e){
                e.printStackTrace();
            }
            int getUsersListPosition = 0;
            if (mrGetTitle.compareToIgnoreCase("Captain") == 0) {
                addrsSearv.setText("Address: "+findMyAddress(mrLt, mrLn));
                chargeSearv.setText("Status: Hiace Car");
                mobLieNoGet = driverPhone;
                if(loc1 != null && loc2 != null){
                    ratingServ.setText("Captain Name: "+driverName+" \nCar distance: "+String.format("%.2f",loc1.distanceTo(loc2))+" m");
                }
                else{
                    ratingServ.setText("Captain Name: "+driverName);
                }
            } else if(mrGetTitle.compareToIgnoreCase("My Position") == 0){
                try{
                    getUsersListPosition = pasngrPhone.indexOf(userInfo.getLocalPhone());
                    if(pickPoint.get(getUsersListPosition).equals("1")){
                        chargeSearv.setText("Name: "+userInfo.getLocalUserName()+"\nStatus: Going");
                    }else{
                        chargeSearv.setText("Name: "+userInfo.getLocalUserName()+"\nStatus: Not going");
                    }
                    ratingServ.setText("Pickup Time: "+pickTime.get(getUsersListPosition));
                    addrsSearv.setText("Address: "+findMyAddress(mrLt, mrLn));
                    mobLieNoGet = pasngrPhone.get(getUsersListPosition);
                }catch (Exception e){
                    Log.d(TAG+"TOUCH_MARKER",""+e.getMessage()+getUsersListPosition+" "+marker.getSnippet().toString());
                }
            }else {
                try{
                    getUsersListPosition = pasngrPhone.indexOf(marker.getSnippet().toString());
                    if(pickPoint.get(getUsersListPosition).equals("1")){
                        chargeSearv.setText("Status: Going");
                    }else{
                        chargeSearv.setText("Status: Not going");
                    }
                    addrsSearv.setText("Address: "+findMyAddress(mrLt, mrLn));
                    mobLieNoGet = pasngrPhone.get(getUsersListPosition);
                    if(loc1 != null && loc2 != null){
                        ratingServ.setText("Pickup time: "+pickTime.get(getUsersListPosition)+" \nCar distance: "+String.format("%.2f",loc1.distanceTo(loc2))+" m");
                    }
                    else{
                        ratingServ.setText("Pickup time: "+pickTime.get(getUsersListPosition));
                    }
                }catch (Exception e){
                    Log.d(TAG+"TOUCH_MARKER",""+e.getMessage()+getUsersListPosition+" "+marker.getSnippet().toString());
                }
            }
            try{
                Log.d(TAG+"_testing_Onloc_change",""+""+userInfo.getLocalUserType()+" "+userInfo.getLocalUserType().equals("0")+" "+
                        userInfo.getLocalUserType().equals("1")+"flag: "+getUserTyepFlag);
            }catch (Exception e){
                chargeSearv.setText("Status: Not found"+e.getMessage());
            }
        } catch (Exception e) {
            ratingServ.setText("Sorry, Failed to detect user");
            addrsSearv.setText("Address: Not found");
            chargeSearv.setText("Status: Not found");
        }
    }
}
