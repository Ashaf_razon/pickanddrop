package com.avis.avistechbd.pickanddrop.backend_work;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import com.avis.avistechbd.pickanddrop.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SendGetLocToFromFirebase {

    private static final String DRIVER_REF = "Captain";
    private static final String PASSENGER_REF = "PassengerList";
    //Set_App_Context
    private Context context;
    //Customer_Local_Database_With_Key<>Value_pair
    private SharedPreferences mySharedPref;
    private String phoneKey = "StorePhone";
    private String nameKey = "StoreName";
    private String uTypeKey = "StoreUtype";
    //FireBase_Instance_initiate
    private FirebaseDatabase database;

    public SendGetLocToFromFirebase(Context context, SharedPreferences mySharedPref, FirebaseDatabase database) {
        this.context = context;
        this.mySharedPref = mySharedPref;
        this.database = database;
    }

    //get_Local_phn_no (AuthPhone number->Create new Acc / ForgotPass)
    public String getLocalPhone(){
        String uPhone = "017000000";
        try {
            uPhone = mySharedPref.getString(phoneKey, uPhone);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uPhone;
    }
    //get_Local_User_name
    public String getLocalUserName(){
        String uName = "Avis";
        try {
            uName = mySharedPref.getString(nameKey, uName);
        } catch (Exception e) {
            Log.d("getLocalNameERROR",""+e.getMessage());
        }
        return uName;
    }
    //get_Local_User_Type
    public String getLocalUserType(){
        String uType = "0";
        try {
            uType = mySharedPref.getString(uTypeKey, uType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uType;
    }

    //set_New_Location_Firebase
    public boolean setCustomerLocationInFireDB(String uPhone,String myLatt,String myLong){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            //Setting_data
            myRef.child("uLatt").setValue(myLatt);
            myRef.child("uLong").setValue(myLong);
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, R.string.set_customer_loc_fire_error, Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //set_Users_Data_Firebase
    public boolean setUsersDataFireDB(String uPhone,String uName,String uGender,String uPass){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            Log.d("snap try",myRef.toString());
            //Setting_data
            myRef.child("uName").setValue(uName);
            myRef.child("uGender").setValue(uGender);
            myRef.child("ePass").setValue(uPass);
            //deFault
            myRef.child("eJob").setValue("Customer");
            myRef.child("eMail").setValue("customer@avis.com");
            myRef.child("sCost").setValue("0-tk");
            myRef.child("uWork").setValue("1");
            myRef.child("uLog").setValue("0");
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Log.d("snap catch",e.getMessage());
            Toast.makeText(context, "Account Create DB Error, Try again latter >> ", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //set_Login_Out_Status
    public boolean setLoginDB(String uPhone, String status){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            //Setting_data
            myRef.child("Status").setValue(status);
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "DB Error, Try again latter", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //set_Going_Status
    public boolean setGoingStatus(String uPhone, String status){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            //Setting_data
            myRef.child("Going").setValue(status);
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "DB Error, Try again latter", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //set_New_Pick_point_Status
    public boolean setNewPickPoint(String ref, String ref2, String uLatt, String uLong){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(ref); //Continues Update Position
            DatabaseReference myRef2 = database.getReference(ref2); //Only Pick Point
            //Continues Update
            myRef.child("uLatt").setValue(uLatt);
            myRef.child("uLong").setValue(uLong);
            ////Only Pick Point
            myRef2.child("uLatt").setValue(uLatt);
            myRef2.child("uLong").setValue(uLong);
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "DB Error, Try again latter", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //update_Position_Status
    public boolean updateUsersPosition(String ref, String uLatt, String uLong){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(ref); //Continues Update Position
            //Continues Update
            myRef.child("uLatt").setValue(uLatt);
            myRef.child("uLong").setValue(uLong);
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            //Toast.makeText(context, "DB Error, Try again latter", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }
}
