package com.avis.avistechbd.pickanddrop.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.avis.avistechbd.pickanddrop.R;
import com.avis.avistechbd.pickanddrop.backend_work.SendGetLocToFromFirebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetPickStatus extends AppCompatActivity {

    //Constant
    private static final String DRIVER_REF = "Captain";
    private static final String PASSENGER_REF = "PassengerList";
    //ViewBind
    @BindView(R.id.mySearchServ)
    Spinner searchServ;
    private String[] workType = {"Select your status","Not going today","Going today"};
    private int flagLocCall;
    private String statusItem = null;
    //ActivityVariables
    private Context context;
    private Handler handler;
    private ProgressDialog pd;
    //FirebaseDB
    private FirebaseDatabase database;
    //LocalDB
    private SharedPreferences mySharedPref;
    private String myRef = "userStatus"; //Reference_Key
    private SendGetLocToFromFirebase userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pick_status);
        ButterKnife.bind(this);
        setTitle("Pickup Status");

        context = SetPickStatus.this;
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(context);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        //Firebase
        database = FirebaseDatabase.getInstance();
        //localDB
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        //getUserPhone
        userInfo = new SendGetLocToFromFirebase(context,mySharedPref,database);
        initSpinnSearch();
    }

    @OnClick(R.id.setYourStatus)
    public void onViewClicked() {
        if(flagLocCall >= 1){
            //setInputStatusTO_firebaseDB
            // 0 = notGoint, 1 = Going
            callFireDatabase(flagLocCall-1);
            Toast.makeText(context,"Your status: "+ statusItem, Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context,"Select your status", Toast.LENGTH_SHORT).show();
        }
    }

    private void initSpinnSearch() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, workType);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        searchServ.setAdapter(dataAdapter);
        searchServ.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    flagLocCall = 0;
                    if (position >= 1) {
                        flagLocCall = position;
                        statusItem  = parent.getItemAtPosition(position).toString();
                    } else {
                        flagLocCall = 0;
                        Toast.makeText(context, R.string.select_worker_warning, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(context, R.string.select_spinner_error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                try {
                    flagLocCall = 0;
                    Toast.makeText(context, R.string.no_worker_selection_msg, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callFireDatabase(final int setGoingStatus){
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    try{
                        Thread.sleep(1000);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    String ref="";
                    ref = "HiaceCar/"+PASSENGER_REF+"/"+userInfo.getLocalPhone();
                    if(userInfo.setGoingStatus(ref, String.valueOf(setGoingStatus))){
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                pd.dismiss();
                                Toast.makeText(context,"Successfully status updated", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else{
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                pd.dismiss();
                                Toast.makeText(context,"Failed, status updated", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
        Log.d("GoingStatusUpdate","End");
    }
}
