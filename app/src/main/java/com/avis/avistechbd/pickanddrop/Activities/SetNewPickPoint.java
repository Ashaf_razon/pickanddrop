package com.avis.avistechbd.pickanddrop.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.pickanddrop.R;
import com.avis.avistechbd.pickanddrop.backend_work.GetLocation;
import com.avis.avistechbd.pickanddrop.backend_work.GetSetUserDataToLocNfirebase;
import com.avis.avistechbd.pickanddrop.backend_work.SendGetLocToFromFirebase;
import com.avis.avistechbd.pickanddrop.get_location_backend.CheckUserPermission;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetNewPickPoint extends AppCompatActivity implements GetLocation {
    //ConstantVar
    private static final String PASSENGER_REF = "PassengerList";
    private static final String PICK_POINT_REF = "PickUpPoint";
    private static final int REQUEST_LOCATION = 1;
    @BindView(R.id.setNewPickPoint)
    Button setNewPickPoint;
    //Variable_Location
    //default = Kawran_Bazar
    private double locLatt = 23.750150;
    private double locLong = 90.391231;

    @BindView(R.id.showLocation)
    TextView showLocation;
    //ActivityVariables
    private Context context;
    private Handler handler;
    private ProgressDialog pd;
    //FirebaseDB
    private FirebaseDatabase database;
    private SendGetLocToFromFirebase userInfo;
    //LocalDB
    private SharedPreferences mySharedPref;
    private String myRef = "userStatus"; //Reference_Key
    private CheckUserPermission getMyPermission; //Permission_Check
    private LocationManager locationManager;
    private GetSetUserDataToLocNfirebase getMyAllLoc; //class_Object

    //address
    private String city, area, add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_new_pick_point);
        ButterKnife.bind(this);
        setTitle("New Pickup Point");

        context = SetNewPickPoint.this;
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(context);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        database = FirebaseDatabase.getInstance();
        //localDB
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        //getUserPhone
        userInfo = new SendGetLocToFromFirebase(context, mySharedPref, database);
        //initially_users_Location_view
        try {
            initLocationManagerPermission();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPermissionInfoNLocation() {
        if (getMyPermission.checkGPSpermission()) {
            //Done_Getting_All_Permission_(Location & GPS)
            try {
                if (getMyPermission.isNetworkAvailable()) {
                    //Get_Location_Once
                    getMyAllLoc.getLocation();
                } else {
                    Toast.makeText(this, R.string.internet_msg, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, R.string.location_fetch_error+" "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.gps_msg, Toast.LENGTH_SHORT).show();
        }
    }

    private void initLocationManagerPermission() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                callForRequestPermission();
                //return;
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.location_manager_init_error, Toast.LENGTH_SHORT).show();
        }
        //get_Location_&_Permission_instance
        getMyAllLoc = new GetSetUserDataToLocNfirebase(locationManager, context, (Activity) context, SetNewPickPoint.this);
        getMyPermission = new CheckUserPermission(context, (Activity) context, locationManager);
    }

    private void callForRequestPermission() {
        ActivityCompat.requestPermissions(SetNewPickPoint.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
    }

    @OnClick({R.id.getNewPickPoint, R.id.setNewPickPoint})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.getNewPickPoint:
                getPermissionInfoNLocation();
                break;
            case R.id.setNewPickPoint:
                callPickPointUpdate(String.valueOf(locLatt), String.valueOf(locLong));
                break;
        }
    }

    private String findMyAddress(double lt, double lng) {
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        add = "Not found";
        locLatt = lt;
        locLong = lng;
        try {
            addresses = gcd.getFromLocation(lt, lng, 1);
            city = addresses.get(0).getLocality();
            area = addresses.get(0).getFeatureName();
            add = addresses.get(0).getSubLocality() + ", " + addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return add;
    }

    @Override
    public void getLocationOnce(String myLatt, String myLong, int didUpdate) {
        try {
            if (didUpdate == 1) {
                showLocation.setText("Your Current Location: \nLatt: " + myLatt + "\nLong: "
                        + myLong + "\n" + findMyAddress(Double.valueOf(myLatt), Double.valueOf(myLong)));
            } else {
                showLocation.setText("Your current location is unable to Trace.");
                setNewPickPoint.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            showLocation.setText("Error: " + e.getMessage());
        }
    }

    private void callPickPointUpdate(final String uLatt, final String uLong) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String ref = "", ref2 = "";
                    ref = "HiaceCar/" + PASSENGER_REF + "/" + userInfo.getLocalPhone();
                    ref2 = "HiaceCar/" + PICK_POINT_REF + "/" + userInfo.getLocalPhone();
                    if (userInfo.setNewPickPoint(ref, ref2, uLatt, uLong)) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                pd.dismiss();
                                Toast.makeText(context, "PickupPoint updated", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                pd.dismiss();
                                Toast.makeText(context, "Failed, PickupPoint updated", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Log.d("GoingStatusUpdate", "End");
    }
}
