package com.avis.avistechbd.pickanddrop.create_view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.avis.avistechbd.pickanddrop.R;
import java.util.ArrayList;

public class ListPassengerView extends RecyclerView.Adapter<ListPassengerView.ViewHolder>{

    private static final String TAG = "recyclerView";
    private ArrayList<String> pasngrName = new ArrayList<>();
    private ArrayList<String> pasngrPhone = new ArrayList<>();
    private ArrayList<String> pickPoint = new ArrayList<>();
    private ArrayList<String> pickTIme = new ArrayList<>();
    private Context context;

    public ListPassengerView(ArrayList<String> pasngrName,ArrayList<String> pasngrPhone, ArrayList<String> pickPoint, ArrayList<String> pickTIme, Context context) {
        this.pasngrName = pasngrName;
        this.pasngrPhone = pasngrPhone;
        this.pickPoint = pickPoint;
        this.pickTIme = pickTIme;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_passenger,viewGroup,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Log.d(TAG,"Bind View Holder Called");
        viewHolder.setUserName.setText(pasngrName.get(i));
        viewHolder.setUuserPhone.setText(pasngrPhone.get(i));
        viewHolder.setUserPickPoint.setText(pickPoint.get(i));
        viewHolder.setUserPickTime.setText(pickTIme.get(i));
    }

    @Override
    public int getItemCount() {
        return pasngrName.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView setUserName, setUuserPhone, setUserPickPoint, setUserPickTime;
        RelativeLayout parentLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            setUserName = itemView.findViewById(R.id.passngName);
            setUuserPhone = itemView.findViewById(R.id.passngPhone);
            setUserPickPoint = itemView.findViewById(R.id.passngPickPoint);
            setUserPickTime = itemView.findViewById(R.id.passngPickTime);
            parentLayout = itemView.findViewById(R.id.relative_Layout_item_list);

        }
    }

}
