package com.avis.avistechbd.pickanddrop.get_location_backend;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.avis.avistechbd.pickanddrop.R;

public class CheckUserPermission {
    /*
        1) Check for global *Fine Loc, *Course Loc [return true/false]
        2) Check for GPS permission & show AlertBuilder to enable GPS [return true/false]
        3) Check Internet State
    */

    private Context context;
    LocationManager locationManager;
    protected Activity getActivity;
    private static boolean checkCommonLocPermission;
    private static boolean checkUserGPSPermission;

    public CheckUserPermission(Context context,Activity getActivity,LocationManager locationManager) {
        this.context = context;
        this.getActivity = getActivity;
        this.locationManager = locationManager;
    }

    //First_Permission
    public boolean primaryPermissionCheck() {
        checkCommonLocPermission = false;
        try{
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkCommonLocPermission = false;
            }else{
                checkCommonLocPermission = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, R.string.init_primary_permission_error,Toast.LENGTH_LONG).show();
        }

        return checkCommonLocPermission;
    }

    //Second_Permission
    public boolean checkGPSpermission(){
        checkUserGPSPermission = false;
        try{
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                checkUserGPSPermission = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, R.string.gps_permission_error,Toast.LENGTH_LONG).show();
        }
        return checkUserGPSPermission;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.gps_msg)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

}
