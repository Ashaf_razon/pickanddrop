package com.avis.avistechbd.pickanddrop.custom_dialog;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.avis.avistechbd.pickanddrop.R;
import com.avis.avistechbd.pickanddrop.backend_work.SendGetLocToFromFirebase;
import com.google.firebase.database.FirebaseDatabase;

public class mDialogClass extends Dialog implements View.OnClickListener {

    private static final String DRIVER_REF = "Captain";
    private static final String PASSENGER_REF = "PassengerList";
    //setStatusFire
    private Context context;
    private FirebaseDatabase database;
    private SharedPreferences mySharedPref;
    private String LOG_OUT_STATUS = "0";
    //Dialog
    public Activity activity;
    public Dialog d;
    public Button yes, no;
    TextView txtDia;
    int dialog_title;
    int flagNo;

    public mDialogClass(Activity activity, int dialog_title, int flagNo, Context context, FirebaseDatabase database, SharedPreferences mySharedPref) {
        super(activity);
        this.activity = activity;
        this.dialog_title = dialog_title;
        this.flagNo = flagNo;
        this.context = context;
        this.database = database;
        this.mySharedPref = mySharedPref;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        txtDia = (TextView) findViewById(R.id.txt_dia);
        txtDia.setText(dialog_title);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                try{
                    methodUpdateStatus();
                }catch (Exception e){
                    Log.d("SetFireStatus",""+e.getMessage());
                }
                activity.finish();
                break;
            case R.id.btn_no:
                dismiss();
                if(flagNo == 3){
                    activity.finish();
                }
                break;
            default:
                break;
        }
        dismiss();
    }

    private void methodUpdateStatus() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                setUpdateStatus(LOG_OUT_STATUS);
            }
        }).start();
    }

    //saveUserStatus
    private void setUpdateStatus(String statusSet){
        SendGetLocToFromFirebase setUserStatus = new SendGetLocToFromFirebase(context,mySharedPref,database);
        String ref="";
        if(setUserStatus.getLocalUserType().equals("1")){
            ref = "HiaceCar/"+PASSENGER_REF+"/";
        }else{
            ref = "HiaceCar/"+DRIVER_REF+"/";
        }
        try{
            if(setUserStatus.setLoginDB(ref+setUserStatus.getLocalPhone(), statusSet)){
                Log.d("SetFireStatus","Status Changed");
            }else{
                Log.d("SetFireStatus","Status not changed");
            }
        }catch (Exception e){
            Log.d("SetFireStatus",""+e.getMessage()+" "+context+" "+mySharedPref+" "+database);
        }
    }
}
