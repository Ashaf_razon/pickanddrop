package com.avis.avistechbd.pickanddrop.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.avis.avistechbd.pickanddrop.R;
import com.avis.avistechbd.pickanddrop.backend_work.SendGetLocToFromFirebase;
import com.avis.avistechbd.pickanddrop.custom_dialog.mDialogClass;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    //Constant
    private static final int REQUEST_LOCATION = 1;
    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private static final int DRIVER = 0;
    private static final int PASSENGER = 1;
    private static final String DRIVER_REF = "Captain";
    private static final String PASSENGER_REF = "PassengerList";
    private String LOG_IN_STATUS = "1";
    //Variable
    private String getUserPhone = null;
    private String getUserId = null;
    private String setUname = "Avis";
    private int flagLog;
    //ViewBinding
    @BindView(R.id.getPhn)
    EditText getPhn;
    @BindView(R.id.getID)
    EditText getID;
    //ActivityVariables
    private Context context;
    private Handler handler;
    private ProgressDialog pd;
    //FirebaseDB
    private FirebaseDatabase database;
    //LocalDB
    private SharedPreferences mySharedPref;
    private String myRef = "userStatus"; //Reference_Key
    private String phoneKey = "StorePhone";
    private String nameKey = "StoreName";
    private String uTypeKey = "StoreUtype";
    private SharedPreferences.Editor editSave;//LoggedIn_phn

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
        context = MainActivity.this;
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(context);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        database = FirebaseDatabase.getInstance();
        //localDB
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        try{
            checkPermission();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        customDialogCall(R.string.dialog_title);
    }

    private void customDialogCall(int setTitle) {
        mDialogClass myDialog = new mDialogClass((Activity) context, setTitle, 1,context,database,mySharedPref);
        try {
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }

    @OnClick({R.id.UserCaptain, R.id.UserPassenger})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //0 driver
            //1 passenger
            case R.id.UserCaptain:
                if(getUserInput()){
                    if (isNetworkAvailable()) {
                        try {
                            if (isGPSEnabled(context)) {
                                //Toast.makeText(context, getUserPhone + " " + getUserId, Toast.LENGTH_SHORT).show();
                                callFireDatabase(DRIVER);
                            } else {
                                Toast.makeText(context, R.string.gps_alert, Toast.LENGTH_SHORT).show();
                            }
                        } catch (SecurityException e) {
                            callForRequestPermission();
                        }
                    } else {
                        Toast.makeText(context, R.string.internet_alert, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.UserPassenger:
                if(getUserInput()){
                    if (isNetworkAvailable()) {
                        try {
                            if (isGPSEnabled(context)) {
                                //Toast.makeText(context, getUserPhone + " " + getUserId, Toast.LENGTH_SHORT).show();
                                //Call MapDrawer
//                                pd.show();
//                                responseNewPage(PASSENGER);
                                //Loading_Auth_newPage
                                callFireDatabase(PASSENGER);
                            } else {
                                Toast.makeText(context, R.string.gps_alert, Toast.LENGTH_SHORT).show();
                            }
                        } catch (SecurityException e) {
                            callForRequestPermission();
                        }
                    } else {
                        Toast.makeText(context, R.string.internet_alert, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    //Call Map
    private void responseNewPage(final int userType) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Intent getMap = new Intent(MainActivity.this, MapDrawer.class);
                        getMap.putExtra("UserType", userType);
                        startActivity(getMap);
                    }
                });
            }
        }).start();
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private void callForRequestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSION_ACCESS_COARSE_LOCATION);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
    }

    public boolean isGPSEnabled(Context mContext) {
        LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void checkPermission() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                callForRequestPermission();
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.location_manager_init_error, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean getUserInput() {
        boolean flagInput = true;
        try {
            getUserPhone = getPhn.getText().toString();
            getUserId = getID.getText().toString();
        } catch (Exception e) {
            Log.d("inputError", "" + e.getMessage());
        }
        if (getUserPhone.length() < 11) {
            flagInput = false;
            getPhn.setError("Set valid office phone no.");
            getPhn.requestFocus();
        }
        if (getUserId.length() < 5) {
            flagInput = false;
            getID.setError("Set valid office ID");
            getID.requestFocus();
        }
        return flagInput;
    }

    private void callFireDatabase(final int userType){
        flagLog = 0;
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    try{
                        Thread.sleep(500);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    DatabaseReference reference = database.getInstance().getReference("HiaceCar");
                    Query query = null;
                    if(userType == 1){
                        query = reference.child(PASSENGER_REF).child(getUserPhone);
                    }else{
                        query = reference.child(DRIVER_REF).child(getUserPhone);
                    }
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                    try{
                                        if(issue.getKey().equals("Name")){
                                            setUname = issue.getValue().toString();
                                        }
                                        Log.d("LogIn","snap_name0: "+setUname);
                                    }catch (Exception e){
                                        Log.d("LogIn","snap_name1: "+e.getMessage());
                                    }
                                    if(issue.getKey().equals("ID") && issue.getValue().toString().equals(getUserId)){
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                try{
                                                    setCustomerLoggedPhoneDB(getUserPhone,setUname,userType);
                                                    setUpdateStatus(userType,LOG_IN_STATUS);
                                                    Toast.makeText(context, "Successfully Logged in", Toast.LENGTH_SHORT).show();
                                                    pd.dismiss();
                                                    responseNewPage(userType);
                                                }catch(Exception e){
                                                    e.printStackTrace();
                                                    pd.dismiss();
                                                }
                                            }
                                        });
                                    }else if(issue.getKey().equals("ID") && !(issue.getValue().toString().equals(getUserId))){
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                try{
                                                    Toast.makeText(context, "Incorrect ID", Toast.LENGTH_SHORT).show();
                                                    pd.dismiss();
                                                }catch(Exception e){
                                                    e.printStackTrace();
                                                    pd.dismiss();
                                                }
                                            }
                                        });
                                    }
                                }
                            }else{
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try{
                                            Toast.makeText(context, "User not exist, Please Register", Toast.LENGTH_SHORT).show();
                                            pd.dismiss();
                                        }catch(Exception e){
                                            e.printStackTrace();
                                            pd.dismiss();
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            pd.dismiss();
                            Toast.makeText(context, "SignIn DB Error, Try again latter ", Toast.LENGTH_SHORT).show();
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
        Log.d("LogIn","End");
    }

    //FireDB
    //saveUserStatus
    private void setUpdateStatus(int uType,String statusSet){
        SendGetLocToFromFirebase setUserStatus = new SendGetLocToFromFirebase(context,mySharedPref,database);
        String ref="";
        if(uType == 1){
            ref = "HiaceCar/"+PASSENGER_REF+"/";
        }else{
            ref = "HiaceCar/"+DRIVER_REF+"/";
        }
        try{
            if(setUserStatus.setLoginDB(ref+setUserStatus.getLocalPhone(), statusSet)){
                Log.d("SetFireStatus","Status Changed");
            }else{
                Log.d("SetFireStatus","Status not changed");
            }
        }catch (Exception e){
            Log.d("SetFireStatus",""+e.getMessage());
        }
    }
    //LocalDB
    //save Phone , UserType -> setUser Status
    private void setCustomerLoggedPhoneDB(String storePhone,String userName, int storeUtype) {
        try{
            editSave = mySharedPref.edit();
            editSave.putString(phoneKey,storePhone);
            editSave.putString(nameKey,userName);
            editSave.putString(uTypeKey, String.valueOf(storeUtype));
            editSave.commit();
            Log.d("LocalDBsuccess",""+storePhone+" "+userName+" "+storeUtype);
        }catch (Exception e){
            Log.d("LocalDBErr",""+e.getMessage());
        }
    }

}
