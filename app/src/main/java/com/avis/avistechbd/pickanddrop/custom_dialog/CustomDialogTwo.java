package com.avis.avistechbd.pickanddrop.custom_dialog;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.avis.avistechbd.pickanddrop.R;

import org.w3c.dom.Text;

public class CustomDialogTwo extends Dialog implements View.OnClickListener {

    public Activity activity;
    public Dialog d;
    public Button yes;
    TextView txtDia;
    int flagNo;
    int dialogTitle;

    public CustomDialogTwo(Activity activity, int dialog_title, int flagNo) {
        super(activity);
        this.activity = activity;
        this.dialogTitle = dialog_title;
        this.flagNo = flagNo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_two);
        yes = (Button) findViewById(R.id.btn_yes);
        txtDia = (TextView) findViewById(R.id.alertText);
        txtDia.setText(dialogTitle);
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
