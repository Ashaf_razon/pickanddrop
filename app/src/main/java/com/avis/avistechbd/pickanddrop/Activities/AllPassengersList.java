package com.avis.avistechbd.pickanddrop.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.pickanddrop.R;
import com.avis.avistechbd.pickanddrop.create_view.ListPassengerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllPassengersList extends AppCompatActivity {
    @BindView(R.id.countAllPassenger)
    TextView countAllPassenger;
    //ActivityVariables
    private Context context;
    private Handler handler;
    private ProgressDialog pd;
    //FirebaseDB
    private FirebaseDatabase database;
    //LocalDB
    private SharedPreferences mySharedPref;
    private String myRef = "userStatus"; //Reference_Key

    //DataArrayList
    private final String TAG = "AllPassengers";
    private ArrayList<String> pasngrName = new ArrayList<>();
    private ArrayList<String> pasngrPhone = new ArrayList<>();
    private ArrayList<String> pickPoint = new ArrayList<>();
    private ArrayList<String> pickTIme = new ArrayList<>();
    //address
    private String city, area, add;
    //PassengerCount

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_passengers_list);
        ButterKnife.bind(this);
        setTitle("Passenger's List");
        //initialize
        context = AllPassengersList.this;
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(context);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        database = FirebaseDatabase.getInstance();
        //localDB
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        callFireListOfPassengers();
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        ListPassengerView adapter = new ListPassengerView(pasngrName, pasngrPhone, pickPoint, pickTIme, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void callFireListOfPassengers() {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DatabaseReference reference = database.getInstance().getReference("HiaceCar");
                    Query query = reference.child("PassengerList");
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                    // Log.d("LogIn","snap_2: "+issue);
                                    if(!issue.child("PickTime").getValue().toString().equals("0.00 am/pm")){
                                        pasngrPhone.add("Phone: " + issue.getKey());
                                        pasngrName.add("Name: " + issue.child("Name").getValue().toString());
                                        pickPoint.add("Pick Point: " + findMyAddress(Double.valueOf(issue.child("uLatt").getValue().toString()), Double.valueOf(issue.child("uLong").getValue().toString())));
                                        pickTIme.add("Pick Time: " + issue.child("PickTime").getValue().toString());
                                    }
                                }
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            countAllPassenger.setText("Total Passenger: "+pasngrPhone.size());
                                            initRecyclerView();
                                            pd.dismiss();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            pd.dismiss();
                                        }
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Toast.makeText(context, "Sorry, No Passenger found", Toast.LENGTH_SHORT).show();
                                            pd.dismiss();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            pd.dismiss();
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            pd.dismiss();
                            Toast.makeText(context, "SignIn DB Error, Try again latter ", Toast.LENGTH_SHORT).show();
                        }
                    });
                    Thread.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Log.d("LogIn", "End");
    }

    private String findMyAddress(double lt, double lng) {
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        add = "Not found";
        try {
            addresses = gcd.getFromLocation(lt, lng, 1);
            city = addresses.get(0).getLocality();
            area = addresses.get(0).getFeatureName();
            add = addresses.get(0).getSubLocality()+ ", " + addresses.get(0).getAddressLine(0) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return add;
    }
}
