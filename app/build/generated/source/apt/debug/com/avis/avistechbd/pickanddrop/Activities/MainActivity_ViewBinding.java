// Generated code from Butter Knife. Do not modify!
package com.avis.avistechbd.pickanddrop.Activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.avis.avistechbd.pickanddrop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view2131230726;

  private View view2131230727;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    target.getPhn = Utils.findRequiredViewAsType(source, R.id.getPhn, "field 'getPhn'", EditText.class);
    target.getID = Utils.findRequiredViewAsType(source, R.id.getID, "field 'getID'", EditText.class);
    view = Utils.findRequiredView(source, R.id.UserCaptain, "method 'onViewClicked'");
    view2131230726 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.UserPassenger, "method 'onViewClicked'");
    view2131230727 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.getPhn = null;
    target.getID = null;

    view2131230726.setOnClickListener(null);
    view2131230726 = null;
    view2131230727.setOnClickListener(null);
    view2131230727 = null;
  }
}
