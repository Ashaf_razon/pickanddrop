// Generated code from Butter Knife. Do not modify!
package com.avis.avistechbd.pickanddrop.Activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.avis.avistechbd.pickanddrop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SetNewPickPoint_ViewBinding implements Unbinder {
  private SetNewPickPoint target;

  private View view2131230913;

  private View view2131230818;

  @UiThread
  public SetNewPickPoint_ViewBinding(SetNewPickPoint target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SetNewPickPoint_ViewBinding(final SetNewPickPoint target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.setNewPickPoint, "field 'setNewPickPoint' and method 'onViewClicked'");
    target.setNewPickPoint = Utils.castView(view, R.id.setNewPickPoint, "field 'setNewPickPoint'", Button.class);
    view2131230913 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.showLocation = Utils.findRequiredViewAsType(source, R.id.showLocation, "field 'showLocation'", TextView.class);
    view = Utils.findRequiredView(source, R.id.getNewPickPoint, "method 'onViewClicked'");
    view2131230818 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SetNewPickPoint target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.setNewPickPoint = null;
    target.showLocation = null;

    view2131230913.setOnClickListener(null);
    view2131230913 = null;
    view2131230818.setOnClickListener(null);
    view2131230818 = null;
  }
}
