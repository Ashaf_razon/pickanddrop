// Generated code from Butter Knife. Do not modify!
package com.avis.avistechbd.pickanddrop.Activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.avis.avistechbd.pickanddrop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MapDrawer_ViewBinding implements Unbinder {
  private MapDrawer target;

  private View view2131230972;

  @UiThread
  public MapDrawer_ViewBinding(MapDrawer target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MapDrawer_ViewBinding(final MapDrawer target, View source) {
    this.target = target;

    View view;
    target.chargeSearv = Utils.findRequiredViewAsType(source, R.id.chargeSearv, "field 'chargeSearv'", TextView.class);
    target.ratingServ = Utils.findRequiredViewAsType(source, R.id.ratingServ, "field 'ratingServ'", TextView.class);
    target.addrsSearv = Utils.findRequiredViewAsType(source, R.id.addrsSearv, "field 'addrsSearv'", TextView.class);
    target.uDetection = Utils.findRequiredViewAsType(source, R.id.u_detection, "field 'uDetection'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.upArraw, "field 'upArraw' and method 'onUpArrawClicked'");
    target.upArraw = Utils.castView(view, R.id.upArraw, "field 'upArraw'", ImageView.class);
    view2131230972 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onUpArrawClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MapDrawer target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.chargeSearv = null;
    target.ratingServ = null;
    target.addrsSearv = null;
    target.uDetection = null;
    target.upArraw = null;

    view2131230972.setOnClickListener(null);
    view2131230972 = null;
  }
}
