// Generated code from Butter Knife. Do not modify!
package com.avis.avistechbd.pickanddrop.Activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.avis.avistechbd.pickanddrop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AllPassengersList_ViewBinding implements Unbinder {
  private AllPassengersList target;

  @UiThread
  public AllPassengersList_ViewBinding(AllPassengersList target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AllPassengersList_ViewBinding(AllPassengersList target, View source) {
    this.target = target;

    target.countAllPassenger = Utils.findRequiredViewAsType(source, R.id.countAllPassenger, "field 'countAllPassenger'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AllPassengersList target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.countAllPassenger = null;
  }
}
