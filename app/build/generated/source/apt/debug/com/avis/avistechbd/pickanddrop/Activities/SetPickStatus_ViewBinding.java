// Generated code from Butter Knife. Do not modify!
package com.avis.avistechbd.pickanddrop.Activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.avis.avistechbd.pickanddrop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SetPickStatus_ViewBinding implements Unbinder {
  private SetPickStatus target;

  private View view2131230914;

  @UiThread
  public SetPickStatus_ViewBinding(SetPickStatus target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SetPickStatus_ViewBinding(final SetPickStatus target, View source) {
    this.target = target;

    View view;
    target.searchServ = Utils.findRequiredViewAsType(source, R.id.mySearchServ, "field 'searchServ'", Spinner.class);
    view = Utils.findRequiredView(source, R.id.setYourStatus, "method 'onViewClicked'");
    view2131230914 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SetPickStatus target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.searchServ = null;

    view2131230914.setOnClickListener(null);
    view2131230914 = null;
  }
}
