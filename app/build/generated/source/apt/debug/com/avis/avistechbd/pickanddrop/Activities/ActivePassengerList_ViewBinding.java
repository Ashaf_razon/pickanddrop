// Generated code from Butter Knife. Do not modify!
package com.avis.avistechbd.pickanddrop.Activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.avis.avistechbd.pickanddrop.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActivePassengerList_ViewBinding implements Unbinder {
  private ActivePassengerList target;

  private View view2131230908;

  private View view2131230909;

  @UiThread
  public ActivePassengerList_ViewBinding(ActivePassengerList target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ActivePassengerList_ViewBinding(final ActivePassengerList target, View source) {
    this.target = target;

    View view;
    target.countGoingPassenger = Utils.findRequiredViewAsType(source, R.id.countGoingPassenger, "field 'countGoingPassenger'", TextView.class);
    target.countNotGoingPassenger = Utils.findRequiredViewAsType(source, R.id.countNotGoingPassenger, "field 'countNotGoingPassenger'", TextView.class);
    view = Utils.findRequiredView(source, R.id.seeGoing, "method 'onViewClicked'");
    view2131230908 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.seeNotGoing, "method 'onViewClicked'");
    view2131230909 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ActivePassengerList target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.countGoingPassenger = null;
    target.countNotGoingPassenger = null;

    view2131230908.setOnClickListener(null);
    view2131230908 = null;
    view2131230909.setOnClickListener(null);
    view2131230909 = null;
  }
}
